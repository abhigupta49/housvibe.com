const express = require("express");

const path = require("path");

const app = express();

require("./db/conn")

const Contact = require("./models/contact");
const {json} = require("express");


const port = process.env.PORT || 3000;


const static_path = path.join(__dirname,"../public");
const template_path = path.join(__dirname,"../templates/views");
const partials_path = path.join(__dirname,"../templates/partials");

app.use(express.json());

app.use(express.urlencoded({extended:false}));
app.use(express.static(static_path));
app.set("view engine","hbs");

app.get("/", (req,res) =>{
    res.render("index");
});

app.get("/index", (req,res) =>{
    res.render("index");
});

app.get("/about-us", (req,res) =>{
    res.render("about-us");
});

app.get("/blog-post", (req,res) =>{
    res.render("blog-post");
});

app.get("/contact-us", (req,res) =>{
    res.render("contact-us");
});

app.post("/contact-us",async(req,res)=>{
    try{
        const firstname=req.body.firstname;
        if(firstname!=''){
            const contactForm = new Contact({
                firstname : req.body.firstname,
                lastname : req.body.lastname,
                email : req.body.email,
                message:req.body.message
            })
            const Contactd =await contactForm.save();
            console.log("save successfully");
            res.status(201).render("index");
            
        }else{
            console.log("can not empty");
        }
    }catch(error){
            res.status(400).send(error);
        }
    })
    

app.get("/faq", (req,res) =>{
    res.render("faq");
});

app.get("/full-width-gallery", (req,res) =>{
    res.render("index");
});

app.get("/grid-blog", (req,res) =>{
    res.render("grid-blog");
});

app.get("/grid-gallery", (req,res) =>{
    res.render("grid-gallery");
});

app.get("/full-width-masonry-gallery", (req,res) =>{
    res.render("full-width-masonry-gallery");
});

app.listen(port, () => {
    console.log(`Server running at port no ${port}`);
});